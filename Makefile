
GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
NAME=leon


all: test build
build:
	$(GOBUILD) -o build/$(NAME)
clean: 
	$(GOCLEAN)
	rm -rf build
run:
	./build/$(NAME)
deps:
