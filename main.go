package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
)

func main() {
	api_token := "311ca9b0-2b9e-4a16-ab17-df2e8bfa7aed"

	fmt.Print("Enter IP Address: ")
	var ipAddress string
	fmt.Scanln(&ipAddress)

	// Code modified from:
	// https://stackoverflow.com/a/24455606

	url := "http://" + ipAddress + ":8080/api/log/report"

	var jsonStr = []byte(`{"api_token":"` + api_token + `"}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)

	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println(string(body))
}
